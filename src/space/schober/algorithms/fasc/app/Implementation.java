package space.schober.algorithms.fasc.app;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import space.schober.algorithms.fasc.container.Item;

public class Implementation {
	public static LinkedList<LinkedList<Item>> findFirst(List<Item> items, long target) {
		LinkedList<LinkedList<Item>> usedItems = new LinkedList<>();
		findFirst(items, 0, usedItems, target);
		return usedItems;
	}

	private static boolean findFirst(List<Item> itemsRemaining, double sum, LinkedList<LinkedList<Item>> usedItems,
			long target) {
		for (int i = 0; i < itemsRemaining.size(); i++) {
			Item selector = itemsRemaining.get(i);
			// System.out.println(selector);
			double newSum = selector.getCost() + sum;

			if (newSum < target) {
				if (itemsRemaining.size() == 1)
					continue;

				// System.out.println("🗸");
				List<Item> temp = new LinkedList<>(itemsRemaining);
				Item removedItem = temp.remove(i);
				if (findFirst(temp, newSum, usedItems, target)) {
					usedItems.getLast().addLast(removedItem);
					return true;
				}
			} else if (newSum == target) {
				usedItems.addLast(new LinkedList<>());
				usedItems.getLast().addLast(selector);
				// System.out.println("✅");
				return true;
			}
		}
		// System.out.println("x");
		return false;
	}

	public static LinkedList<LinkedList<Item>> findAll(List<Item> items, long target) {
		return findAll(items, 0, new LinkedList<>(), new LinkedList<>(), target);
	}

	private static LinkedList<LinkedList<Item>> findAll(List<Item> itemsRemaining, double sum,
			LinkedList<Item> removedItems, LinkedList<LinkedList<Item>> used, long target) {

		for (int i = 0; i < itemsRemaining.size(); i++) {
			Item selector = itemsRemaining.get(i);
			double newSum = selector.getCost() + sum;

			if (newSum < target) {
				if (itemsRemaining.size() == 1)
					continue;

				List<Item> tempRemaining = new LinkedList<>(itemsRemaining);
				LinkedList<Item> tempRemoved = new LinkedList<>(removedItems);
				Item removedItem = tempRemaining.remove(i);
				tempRemoved.addLast(removedItem);
				findAll(tempRemaining, newSum, tempRemoved, used, target);
			} else if (newSum == target) {
				used.addLast(new LinkedList<>(removedItems));
				used.getLast().addLast(selector);
			}
		}
		return used;
	}

	public static Set<LinkedList<Item>> findAllSet(List<Item> items, long target) {
		return findAllSet(items, 0, new LinkedList<>(), new HashSet<>(), target);
	}

	private static Set<LinkedList<Item>> findAllSet(List<Item> itemsRemaining, double sum,
			LinkedList<Item> removedItems, HashSet<LinkedList<Item>> used, long target) {

		for (int i = 0; i < itemsRemaining.size(); i++) {
			Item selector = itemsRemaining.get(i);
			double newSum = selector.getCost() + sum;

			if (newSum < target) {
				if (itemsRemaining.size() == 1)
					continue;

				List<Item> tempRemaining = new LinkedList<>(itemsRemaining);
				LinkedList<Item> tempRemoved = new LinkedList<>(removedItems);
				Item removedItem = tempRemaining.remove(i);
				tempRemoved.addLast(removedItem);
				findAllSet(tempRemaining, newSum, tempRemoved, used, target);
			} else if (newSum == target) {
				removedItems.add(selector);
				used.add(removedItems);
			}
		}
		return used;
	}
}
