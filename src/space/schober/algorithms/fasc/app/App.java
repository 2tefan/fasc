package space.schober.algorithms.fasc.app;

import java.util.List;

import space.schober.algorithms.fasc.container.Item;

public class App {
	public static void main(String[] args) {
		List<Item> items = Helper.getLongerExampleList();

		// Item.printItems(Implementation.findFirst(items, 169 + 30));
		Item.printItems(Implementation.findAll(items, 169 + 30));
		//Item.printItems(Implementation.findAllSet(items, 169 + 30));
	}
}
