package space.schober.algorithms.fasc.app;

import java.util.ArrayList;
import java.util.List;

import space.schober.algorithms.fasc.container.Item;

public class Helper {
	public static List<Item> getExampleList() {
		List<Item> items = new ArrayList<>();
		items.add(new Item("Dog leather parka", 7.48));
		items.add(new Item("Plan button-down shirt", 12));
		items.add(new Item("Patch pants", 34));
		items.add(new Item("Patch T-shirt", 43));
		items.add(new Item("Foxfur pants", 48));
		items.add(new Item("Patch button-down shirt", 67));
		items.add(new Item("Muffalo wool button-down shirt", 68));
		items.add(new Item("Muffalo wool button-down shirt", 68));
		items.add(new Item("Plain pants", 77));
		items.add(new Item("Camelhide bds", 78));
		items.add(new Item("Muffalo wool bds", 91));
		items.add(new Item("Alpaca wool pants", 109));
		items.add(new Item("Muffalo wool parka", 127));
		items.add(new Item("Muffalo wool parka", 127));
		items.add(new Item("Muffalo wool parka", 127));
		items.add(new Item("Muffalo wool parka", 127));
		items.add(new Item("Alpaca wool bds", 155));
		items.add(new Item("Sheep wool parka", 159));
		items.add(new Item("Sheep wool parka", 159));
		items.add(new Item("Sheep wool parka", 159));
		items.add(new Item("Muffalo wool parka", 169));
		items.add(new Item("Alpaca wool parka", 231));
		items.add(new Item("Alpaca wool parka", 231));
		return items;
	}

	public static List<Item> getLongerExampleList() {
		List<Item> items = new ArrayList<>();
		for (int i = 0; i < 2; i++) {
			items.addAll(getExampleList());
			//items.add(new Item("TeSt", (Math.random() * i * 20) % 53));
		}
		return items;
	}
}
