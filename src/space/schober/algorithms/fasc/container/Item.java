package space.schober.algorithms.fasc.container;

import java.util.LinkedList;
import java.util.Set;

public class Item {
	private String name;
	private double cost;

	public Item(String name, double cost) {
		this.setName(name);
		this.setCost(cost);
	}

	@Override
	public String toString() {
		return String.format("[%03.2f$] %s", this.getCost(), this.getName());
	}
	
	public static void printItems(LinkedList<LinkedList<Item>> allPaths) {
		for (LinkedList<Item> items : allPaths) {
			System.out.println(items);
		}
		System.out.println(allPaths.size());
	}
	
	public static void printItems(Set<LinkedList<Item>> allPaths) {
		for (LinkedList<Item> items : allPaths) {
			System.out.println(items);
		}
		System.out.println(allPaths.size());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}
}
