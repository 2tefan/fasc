/**
 * <h1>Fasc (Find All Sum Combinations)</h1>
 * 
 * <p>
 * This package implements an algorithm to find combinations of items to match a
 * specific sum.
 * </p>
 * 
 * <h2>Example use case</h2>
 * <p>
 * To find in <a href="https://rimworldgame.com/">Rimworld</a> a combination of
 * items, which your trade-partner can afford.
 * </p>
 *
 */
package space.schober.algorithms.fasc;